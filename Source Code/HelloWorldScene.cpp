#include "HelloWorldScene.h"

using namespace cocos2d;

CCScene* HelloWorld::scene()
{
    CCScene * scene = NULL;
    do 
    {
        scene = CCScene::create();
        CC_BREAK_IF(! scene);

        HelloWorld *layer = HelloWorld::create();
        CC_BREAK_IF(! layer);

        scene->addChild(layer);
    } while (0);

    return scene;
}

void HelloWorld::timerMethod(float dt){
	if(totalObject>=1){
		if(timer >= 0){
			timer -= 0.39;
			this->removeChild(countdown,true);
			countdown = CCSprite::create("Images/bardl_waktu.dst",CCRectMake(0,0,timer,35));
			countdown->setAnchorPoint(ccp(0,0));
			countdown->setPosition( ccp(winSize.width/2 - 340, winSize.height - 55));
			this->addChild(countdown,2);
		}
	}
}

bool HelloWorld::init()
{
	bool bRet = false;
	srand(time(NULL));
	jumlahOil = 20; totalOil = 0; finishedObject = 1;maxObject = 8;
	totalObject = 0; totalShip = 0; totalCar = 0;totalPlane = 0;
	spawnSpeed = 10; next = false; score = 0;
	timer = 680;
	activatedOil = 999;
	this->schedule( schedule_selector(HelloWorld::timerMethod),0.1 );
	this->schedule( schedule_selector(HelloWorld::gameLogic),0.5);
	if ( CCLayerColor::initWithColor( ccc4(255,255,255,255) ) )
    do 
    {
        CC_BREAK_IF(! CCLayer::init());
		CCMenuItemImage *pCloseItem = CCMenuItemImage::create("Images/CloseNormal.png","Images/CloseSelected.png",this,menu_selector(HelloWorld::menuCloseCallback));
        CC_BREAK_IF(! pCloseItem);

        pCloseItem->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width - 20, CCDirector::sharedDirector()->getWinSize().height - 20));

        CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
        pMenu->setPosition(CCPointZero);
        CC_BREAK_IF(! pMenu);
        this->addChild(pMenu, 1);

		winSize = CCDirector::sharedDirector()->getWinSize();
		CCSprite *bg = CCSprite::create("Images/Background.dst");
		bg->setPosition( ccp(bg->getContentSize().width/2, winSize.height/2) );
		this->addChild(bg);

		CCSprite *time_bar = CCSprite::create("Images/bar_waktu.dst",CCRectMake(0,0,700,40));
		time_bar->setPosition( ccp(winSize.width/2, winSize.height - 35) );
		this->addChild(time_bar,3);

		countdown = CCSprite::create("Images/bardl_waktu.dst",CCRectMake(0,0,timer,35));
		countdown->setAnchorPoint(ccp(0,0));
		countdown->setPosition( ccp(winSize.width/2 - 340, winSize.height - 55) );
		this->addChild(countdown,2);

		CCSprite* jam = CCSprite::create("Images/bar_waktu_jam.dst",CCRectMake(0,0,42,50));
		jam->setAnchorPoint(ccp(0,0));
		jam->setPosition( ccp(winSize.width/2 - 400, winSize.height - 55) );
		this->addChild(jam,2);

		CCLabelTTF* ttf1 = CCLabelTTF::create("Score : ", "Helvetica" , 40, CCSizeMake(245,40), kCCTextAlignmentCenter);
		ttf1->setPosition(ccp(ttf1->getContentSize().width/2 - 50 , winSize.height-25) );
		this->addChild(ttf1);

		nilai = CCLabelTTF::create("0", "Helvetica" , 40, CCSizeMake(245,40), kCCTextAlignmentCenter);
		nilai->setPosition(ccp(nilai->getContentSize().width/2 , winSize.height-65) );
		this->addChild(nilai);

		this->setTouchEnabled(true);
		this->initChar();

        bRet = true;
    } while (0);

    return bRet;
}

void HelloWorld::initChar(){
	for(int i=0;i<maxObject;i++){
		CCSprite* dummy = CCSprite::create("Images/bensin_lompat.png",CCRectMake(0,0,51,38));
		dummy->setPosition(ccp(-100,-100));
		this->addChild(dummy);
		vhcl[i].karakter = dummy;
		vhcl[i].needed = 10;
	}
}

void HelloWorld::menuCloseCallback(CCObject* pSender)
{
    CCDirector::sharedDirector()->end();
}

void HelloWorld::levelGenerator(){
	if(finishedObject < maxObject){
		finishedObject++;
	}else if(spawnSpeed > 5) {
		spawnSpeed -= 1;
	}
	next = false;
}

void HelloWorld::gameLogic(float dt)
{
	if(next == true){
		this->levelGenerator();
		this->tampilScore();
		next = false;
	}
	this->oilLoop();
	if(totalOil<jumlahOil){
		this->addTarget();
	}
	else if(totalOil == jumlahOil){
		if(totalObject < finishedObject){
			totalObject++;
			this->callObject();
		}	
	} 
}

void HelloWorld::tampilScore(){
	char buffer[10];
	CCLabelTTF* label = nilai;
	this->removeChild(label,true);
	itoa(score,buffer,10);
	nilai = CCLabelTTF::create(buffer, "Helvetica" , 40, CCSizeMake(245,40), kCCTextAlignmentCenter);
	nilai->setPosition(ccp(nilai->getContentSize().width/2 , winSize.height-65) );
	this->addChild(nilai);
}

void HelloWorld::oilLoop(){
	for(int i=0;i<totalOil;i++){
		if(i != activatedOil){
			this->oilAlwaysMove(oil[i].karakter,i);
		} else{
			this->oilSuddenStop(oil[i].karakter,i);
		}
	}
}

void HelloWorld::callObject(){
	int i= rand()%3;
	if (i == 0 && totalShip < 3){
		totalShip++;
		this->spawnShip();
	}
	else if(i == 1 && totalCar < 3){
		totalCar++;
		this->spawnCar();
	}
	else if(i == 2 && totalPlane < 2){
		totalPlane++;
		this->spawnPlane();
	}
	else {
		this->callObject();
	}
}

void HelloWorld::spawnCar(){
	CCSprite *target = CCSprite::create("Images/truk_kedip.png",CCRectMake(0,0,100,75));
	CCSprite *indicateBar = CCSprite::create("Images/bar_indikasi.dst",CCRectMake(0,0,99,20));
	target->setPosition(ccp(winSize.width + target->getContentSize().width,200) );
	indicateBar->setPosition(ccp(winSize.width + target->getContentSize().width,205 + target->getContentSize().height/2) );
    this->addChild(target,2);
	if (totalCar==1){
		if(!vhcl[3].karakter){
			this->removeChild(vhcl[3].karakter,true);
		}
		target->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 250,75)));
		vhcl[3].karakter = target;
		vhcl[3].fuelLayout = indicateBar;
		vhcl[3].fuelLayout->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 250,80 + vhcl[3].karakter->getContentSize().height/2)));
		this->addChild(vhcl[3].fuelLayout,2);
		vhcl[3].needed = 0;
		vhcl[3].doc = 1;
		vhcl[3].fuel = 0;
	}
	else if(totalCar==2){
		if(!vhcl[4].karakter){
			this->removeChild(vhcl[4].karakter,true);
		}
		target->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 100,75)));
		vhcl[4].karakter = target;
		vhcl[4].fuelLayout = indicateBar;
		vhcl[4].fuelLayout->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 100,80 + vhcl[4].karakter->getContentSize().height/2)));
		this->addChild(vhcl[4].fuelLayout,2);
		vhcl[4].needed = 0;
		vhcl[4].doc = 2;
		vhcl[4].fuel = 0;
	}
	else{
		if(!vhcl[5].karakter){
			this->removeChild(vhcl[5].karakter,true);
		}
		target->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 150,160)));
		vhcl[5].karakter = target;
		vhcl[5].fuelLayout = indicateBar;
		vhcl[5].fuelLayout->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 150,165 + vhcl[5].karakter->getContentSize().height/2)));
		this->addChild(vhcl[5].fuelLayout,2);
		vhcl[5].needed = 0;
		vhcl[5].doc = 2;
		vhcl[5].fuel = 0;
	}
}

void HelloWorld::spawnPlane(){
	CCSprite *target = CCSprite::create("Images/pesawat_kedip.png",CCRectMake(0,0,200,150));
	CCSprite *indicateBar = CCSprite::create("Images/bar_indikasi.dst",CCRectMake(0,0,99,20));
	target->setPosition(ccp(winSize.width + target->getContentSize().width,winSize.height -390) );
	indicateBar->setPosition(ccp(winSize.width + target->getContentSize().width,winSize.height - 390 + target->getContentSize().height/2) );
    this->addChild(target,2);
	if (totalPlane==1){
		if(!vhcl[6].karakter){
			this->removeChild(vhcl[6].karakter,true);
		}
		target->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 300,winSize.height-350)));
		vhcl[6].karakter = target;
		vhcl[6].fuelLayout = indicateBar;
		vhcl[6].fuelLayout->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 300,winSize.height - 350 + vhcl[6].karakter->getContentSize().height/2)));
		this->addChild(vhcl[6].fuelLayout,2);
		vhcl[6].needed = 2;
		vhcl[6].doc = 1;
		vhcl[6].fuel = 0;
	}
	else{
		if(!vhcl[7].karakter){
			this->removeChild(vhcl[7].karakter,true);
		}
		target->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 100,winSize.height-425)));
		vhcl[7].karakter = target;
		vhcl[7].fuelLayout = indicateBar;
		vhcl[7].fuelLayout->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(winSize.width - 100,winSize.height - 425 + vhcl[7].karakter->getContentSize().height/2)));
		this->addChild(vhcl[7].fuelLayout,2);
		vhcl[7].needed = 2;
		vhcl[7].doc = 2;
		vhcl[7].fuel = 0;
	}
}

void HelloWorld::spawnShip(){
	CCSprite *target = CCSprite::create("Images/kapallaut_kedip.png",CCRectMake(0,0,200,150));
	CCSprite *indicateBar = CCSprite::create("Images/bar_indikasi.dst",CCRectMake(0,0,99,20));
	target->setPosition(ccp(0 - target->getContentSize().width,300) );
	indicateBar->setPosition(ccp(0 - target->getContentSize().width,350 + target->getContentSize().height/2) );
    this->addChild(target,2);
	CCFiniteTimeAction* actionMove;
	if(totalShip==1){
		if(!vhcl[0].karakter){
			this->removeChild(vhcl[0].karakter,true);
		}
		target->setFlipX(true);
		target->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(175,400)));
		vhcl[0].karakter = target;
		vhcl[0].fuelLayout = indicateBar;
		vhcl[0].fuelLayout->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(175,400 + vhcl[0].karakter->getContentSize().height/2)));
		this->addChild(vhcl[0].fuelLayout,2);
		vhcl[0].needed = 1;
		vhcl[0].doc = 1;
		vhcl[0].fuel = 0;
	}
	else if(totalShip==2){
		if(!vhcl[1].karakter){
			this->removeChild(vhcl[1].karakter,true);
		}
		target->setFlipX(true);
		target->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(300,250)));
		vhcl[1].karakter = target;
		vhcl[1].fuelLayout = indicateBar;
		vhcl[1].fuelLayout->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(300,250 + vhcl[1].karakter->getContentSize().height/2)));
		this->addChild(vhcl[1].fuelLayout,2);
		vhcl[1].needed = 1;
		vhcl[1].doc = 2;
		vhcl[1].fuel = 0;
	}
	else{
		if(!vhcl[2].karakter){
			this->removeChild(vhcl[2].karakter,true);
		}
		target->setFlipX(true);
		target->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(100,200)));
		vhcl[2].karakter = target;
		vhcl[2].fuelLayout = indicateBar;
		vhcl[2].fuelLayout->runAction(CCMoveTo::create((float)spawnSpeed/10,ccp(100,200 + vhcl[2].karakter->getContentSize().height/2)));
		this->addChild(vhcl[2].fuelLayout,2);
		vhcl[2].needed = 1;
		vhcl[2].doc = 2;
		vhcl[2].fuel = 0;
	}
}

void HelloWorld::addTarget(){
	CCSprite *target; 
	int kondisi = rand()%3;
	if (kondisi == 0){
		target = CCSprite::create("Images/bensin_lompat.png",CCRectMake(0,0,102,76));
		target->setAnchorPoint(ccp(0,0));
		oil[totalOil].color = 0;
	}
	else if (kondisi == 1){
		target = CCSprite::create("Images/solar_lompat.png",CCRectMake(0,0,102,76));
		target->setAnchorPoint(ccp(0,0));
		oil[totalOil].color = 1;
	}	
	else if (kondisi == 2){
		target = CCSprite::create("Images/avtur_lompat1.oil",CCRectMake(0,0,102,76));
		target->setAnchorPoint(ccp(0,0));
		oil[totalOil].color = 2;
	} else {
		this->addTarget();
	}
	oil[totalOil].karakter = target;
	target->setPosition(ccp(winSize.width / 2,winSize.height/2) );
    this->addChild(target,3);
	this->oilAlwaysMove(target,totalOil);
	totalOil++;
}

void HelloWorld::addNewOil(){ 
	CCSprite* target;
	int kondisi = rand()%3;
	if (kondisi == 0){
		target = CCSprite::create("Images/bensin_lompat.png",CCRectMake(0,0,102,76));
		target->setAnchorPoint(ccp(0,0));
		oil[activatedOil].karakter = target;
		oil[activatedOil].color = 0;
	}
	else if (kondisi == 1){
		target = CCSprite::create("Images/solar_lompat.png",CCRectMake(0,0,102,76));
		target->setAnchorPoint(ccp(0,0));
		oil[activatedOil].karakter = target;
		oil[activatedOil].color = 1;
	}	
	else if (kondisi == 2){
		target = CCSprite::create("Images/avtur_lompat1.oil",CCRectMake(0,0,102,76));
		target->setAnchorPoint(ccp(0,0));
		oil[activatedOil].karakter = target;
		oil[activatedOil].color = 2;
	} else {
		this->addTarget();
	}
	oil[activatedOil].karakter = target;
	oil[activatedOil].karakter->setPosition(ccp(winSize.width / 2,winSize.height/2) );
    this->addChild(oil[activatedOil].karakter,2);
	this->oilAlwaysMove(oil[activatedOil].karakter,activatedOil);
}

void HelloWorld::oilAlwaysMove(CCSprite* sender,int arr){
	
	int minDuration = (int)2.0;
    int maxDuration = (int)4.0;
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (rand() % rangeDuration ) + minDuration;
	int gerakX = (rand()%600)-300;
	int gerakY = (rand()%800)-400;
	CCSprite *sprite = (CCSprite *)sender;
	sprite->runAction(CCSequence::create(/*CCAnimate::create(anim),*/CCMoveTo::create( (float)actualDuration,ccp((winSize.width/2)-gerakX,(winSize.height/2)-gerakY)),NULL));
}
void HelloWorld::oilSuddenStop(CCSprite* sender,int arr){
	CCSprite *sprite = (CCSprite *)sender;
	sprite->stopAllActions();
}

void HelloWorld::spriteMoveFinished(CCNode* sender)
{
  CCSprite *sprite = (CCSprite *)sender;
  this->removeChild(sprite, true);
}

void HelloWorld::happyOil(){
	float x = oil[activatedOil].karakter->getPositionX();
	float y = oil[activatedOil].karakter->getPositionY();
	if(oil[activatedOil].color == 2){
		this->removeChild(oil[activatedOil].karakter,true);
		oil[activatedOil].karakter = CCSprite::create("Images/avtur_melongo.PNG",CCRectMake(102,0,102,150));
		oil[activatedOil].karakter->setPosition(ccp(x,y));
		this->addChild(oil[activatedOil].karakter,5);
	} else if (oil[activatedOil].color == 1){
		this->removeChild(oil[activatedOil].karakter,true);
		oil[activatedOil].karakter = CCSprite::create("Images/solar_melongo.PNG",CCRectMake(102,0,102,150));
		oil[activatedOil].karakter->setPosition(ccp(x,y));
		this->addChild(oil[activatedOil].karakter,5);
	} else if (oil[activatedOil].color == 0){
		this->removeChild(oil[activatedOil].karakter,true);
		oil[activatedOil].karakter = CCSprite::create("Images/bensin_melongo.PNG",CCRectMake(102,0,102,150));
		oil[activatedOil].karakter->setPosition(ccp(x,y));
		this->addChild(oil[activatedOil].karakter,5);
	}
}

void HelloWorld::removeOil(){
	float x = oil[activatedOil].karakter->getPositionX();
	float y = oil[activatedOil].karakter->getPositionY();
	if(oil[activatedOil].color == 2){
		this->removeChild(oil[activatedOil].karakter,true);
		oil[activatedOil].karakter = CCSprite::create("Images/avtur_senyum.PNG",CCRectMake(102,0,102,150));
		oil[activatedOil].karakter->setPosition(ccp(x,y));
		this->addChild(oil[activatedOil].karakter,5);
	} else if (oil[activatedOil].color == 1){
		this->removeChild(oil[activatedOil].karakter,true);
		oil[activatedOil].karakter = CCSprite::create("Images/solar_senyum.PNG",CCRectMake(102,0,102,150));
		oil[activatedOil].karakter->setPosition(ccp(x,y));
		this->addChild(oil[activatedOil].karakter,5);
	} else if (oil[activatedOil].color == 0){
		this->removeChild(oil[activatedOil].karakter,true);
		oil[activatedOil].karakter = CCSprite::create("Images/bensin_senyum.PNG",CCRectMake(102,0,102,150));
		oil[activatedOil].karakter->setPosition(ccp(x,y));
		this->addChild(oil[activatedOil].karakter,5);
	}
	oil[activatedOil].karakter->runAction(CCSequence::create(CCScaleTo::create(0.2,0,0),CCCallFuncN::create(this,callfuncN_selector(HelloWorld::spriteMoveFinished)),NULL));
}

void HelloWorld::ccTouchesBegan(CCSet* touches, CCEvent* event){
	CCTouch *touch = (CCTouch*)(touches->anyObject());
	CCPoint location = touch->locationInView();
	location = CCDirector::sharedDirector()->convertToGL(location);
	this->cekKlik(location);
	if(activatedOil != 999){
		this->happyOil();
		oil[activatedOil].karakter->runAction(CCScaleBy::create((float)0.1,(float)0.8,(float)0.8));
	}
}

void HelloWorld::ccTouchesMoved(CCSet* touches, CCEvent* event){
	CCTouch *touch = (CCTouch*)(touches->anyObject());
	CCPoint location = touch->locationInView();
	location = CCDirector::sharedDirector()->convertToGL(location);
	if(activatedOil != 999){
		if(location.x < ((winSize.width/2)-300) || location.x > ((winSize.width/2)+300)){
			for(int i = 0;i < maxObject ;i++){
				if(vhcl[i].needed != 10){
					if(oil[activatedOil].karakter->boundingBox().containsPoint(location) == vhcl[i].karakter->boundingBox().containsPoint(location)){
						if(oil[activatedOil].color == vhcl[i].needed){
							this->ekspresiSenang(i);
						} else {
							this->ekspresiSuram(i);
						}
					} else {
						this->ekspresiNormal(i);
					}
				}
			}
		}
		oil[activatedOil].karakter->setPosition(location);
	}
}

void HelloWorld::ccTouchesEnded(CCSet* touches, CCEvent* event){
	CCTouch *touch = (CCTouch*)(touches->anyObject());
	CCPoint location = touch->locationInView();
	location = CCDirector::sharedDirector()->convertToGL(location);
	if(activatedOil != 999){
		if(location.x < ((winSize.width/2)-300) || location.x > ((winSize.width/2)+300)){
			for(int i = 0;i < maxObject;i++){
				if(oil[activatedOil].karakter->boundingBox().containsPoint(location) == vhcl[i].karakter->boundingBox().containsPoint(location)){
					if(oil[activatedOil].color == vhcl[i].needed){
						this->pumpUpFuel(i);
					}
				}
				if(vhcl[i].fuelLayout != NULL){
					this->ekspresiNormal(i);
				}
			}
		}
		this->removeOil();
		this->addNewOil();
		activatedOil = 999;
	}
}

void HelloWorld::ekspresiSenang(int arr){
	float x = vhcl[arr].karakter->getPositionX();
	float y = vhcl[arr].karakter->getPositionY();
	if(arr >= 6){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/pesawat_senang.PNG",CCRectMake(200,0,200,150));
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	} else if (arr <3){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/kapallaut_senang.PNG",CCRectMake(200,0,200,150));
		vhcl[arr].karakter->setFlipX(true);
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	} else if (arr >= 3 && arr < 6){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/truk_senang.PNG",CCRectMake(100,0,100,75));
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	}
}

void HelloWorld::ekspresiSuram(int arr){
	float x = vhcl[arr].karakter->getPositionX();
	float y = vhcl[arr].karakter->getPositionY();
	if(arr >= 6){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/pesawat_panik.PNG",CCRectMake(0,0,200,150));
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	} else if (arr <3){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/kapallaut_panik.PNG",CCRectMake(0,0,200,150));
		vhcl[arr].karakter->setFlipX(true);
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	} else if (arr >= 3 && arr < 6){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/truk_panik.PNG",CCRectMake(0,0,100,75));
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	}
}

void HelloWorld::ekspresiNormal(int arr){
	float x = vhcl[arr].karakter->getPositionX();
	float y = vhcl[arr].karakter->getPositionY();
	if(arr >= 6){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/pesawat_kedip.PNG",CCRectMake(0,0,200,150));
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	} else if (arr <3){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/kapallaut_kedip.PNG",CCRectMake(200,0,200,150));
		vhcl[arr].karakter->setFlipX(true);
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	} else if (arr >= 3 && arr < 6){
		this->removeChild(vhcl[arr].karakter,true);
		vhcl[arr].karakter = CCSprite::create("Images/truk_kedip.PNG",CCRectMake(100,0,100,75));
		vhcl[arr].karakter->setPosition(ccp(x,y));
		this->addChild(vhcl[arr].karakter,1);
	}
}

void HelloWorld::pumpUpFuel(int arr){
	vhcl[arr].fuel++;
	CCLog("FUEL !");
	if (vhcl[arr].fuel == 1){
		if(!vhcl[arr].fuelBar){
			this->removeChild(vhcl[arr].fuelBar,true);
		}
		vhcl[arr].fuelBar = CCSprite::create("Images/bardl_indikasi.dst",CCRectMake(0,0,33,18));
	}
	else if(vhcl[arr].fuel == 2 || vhcl[arr].fuel == 3){
		this->removeChild(vhcl[arr].fuelBar,true);
		vhcl[arr].fuelBar = CCSprite::create("Images/bardl_indikasi.dst",CCRectMake(0,0,32 * vhcl[arr].fuel,18));
	}
	vhcl[arr].fuelBar->setAnchorPoint(ccp(0,0));
	switch (arr){
		case 0 : vhcl[arr].fuelBar->setPosition(ccp(127,390 + vhcl[arr].karakter->getContentSize().height/2));break;
		case 1 : vhcl[arr].fuelBar->setPosition(ccp(253,240 + vhcl[arr].karakter->getContentSize().height/2));break;
		case 2 : vhcl[arr].fuelBar->setPosition(ccp(53,190 + vhcl[arr].karakter->getContentSize().height/2));break;
		case 3 : vhcl[arr].fuelBar->setPosition(ccp(winSize.width - 298,70 + vhcl[3].karakter->getContentSize().height/2));break;
		case 4 : vhcl[arr].fuelBar->setPosition(ccp(winSize.width - 148,70 + vhcl[4].karakter->getContentSize().height/2));break;
		case 5 : vhcl[arr].fuelBar->setPosition(ccp(winSize.width - 198,155 + vhcl[5].karakter->getContentSize().height/2));break;
		case 6 : vhcl[arr].fuelBar->setPosition(ccp(winSize.width - 348,winSize.height - 360 + vhcl[6].karakter->getContentSize().height/2));break;
		case 7 : vhcl[arr].fuelBar->setPosition(ccp(winSize.width - 148,winSize.height - 435 + vhcl[7].karakter->getContentSize().height/2));break;
		default : break;
	}
	this->addChild(vhcl[arr].fuelBar,1);
	
	if(vhcl[arr].fuel==3){
		this->fuelCek(arr);
	}
}

void HelloWorld::fuelCek(int i){
	score++;
	this->removeChild(vhcl[i].fuelLayout,true);
	this->removeChild(vhcl[i].fuelBar,true);
	next = true;
	if(i>=3 && i < 5){
		this->CarGo(i);
	}
	else if(i>=6){
		this->PlaneGo(i);
	}
	else if(i<3){
		this->ShipGo(i);
	}
	totalObject--;
	int j=i+1;
	if(j%3 == 1){
		//BUG 1 FIXED AREA Bensin muncul tiba2
		bool status = false;
		bool status2 = false;
		if(i==0 && totalShip == 2){
			status = true;
		}
		else if(i==3 && totalCar == 2){
			status = true;
		}
		else if(i==6 && totalPlane == 2){
			status = true;
		}
		if(i==0 && totalShip == 3){
			status2 = true;
		}
		else if(i==3 && totalCar == 3){
			status2 = true;
		}
		// Bug Fixed Area End Line
		if(status){
			vhcl[i].karakter = vhcl[j].karakter;
			vhcl[i].needed = vhcl[j].needed;
			vhcl[i].fuel = vhcl[j].fuel;
			vhcl[i].doc = vhcl[j].doc;
			vhcl[i].fuelLayout = vhcl[j].fuelLayout;
			if(vhcl[j].fuel != 0){
				vhcl[i].fuelBar = vhcl[j].fuelBar;
			}
			status = false;
			if(i>=3 && i < 5){
				vhcl[i].karakter->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 250,75)));
				vhcl[i].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 250,80 + vhcl[i].karakter->getContentSize().height/2)));
				if(vhcl[i].fuel != 0){
					vhcl[i].fuelBar->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 298,70 + vhcl[i].karakter->getContentSize().height/2)));
				}
				totalCar--;
			}
			else if(i>=6){
				vhcl[i].karakter->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 300,winSize.height-350)));
				vhcl[i].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 300,winSize.height - 350 + vhcl[i].karakter->getContentSize().height/2)));
				if(vhcl[i].fuel != 0){
					vhcl[i].fuelBar->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 348,winSize.height - 360 + vhcl[i].karakter->getContentSize().height/2)));
				}
				totalPlane--;
			}	
			else if(i<3){
				vhcl[i].karakter->runAction(CCMoveTo::create(0.5,ccp(175,400)));
				vhcl[i].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(175,400 + vhcl[i].karakter->getContentSize().height/2)));
				if(vhcl[i].fuel != 0){
					vhcl[i].fuelBar->runAction(CCMoveTo::create(0.5,ccp(127,390 + vhcl[i].karakter->getContentSize().height/2)));
				}
				totalShip--;
			}
		}
		else if(status2){
			int h = j+1;
			vhcl[i].karakter = vhcl[j].karakter;
			vhcl[i].needed = vhcl[j].needed;
			vhcl[i].fuel = vhcl[j].fuel;
			vhcl[i].doc = vhcl[j].doc;
			vhcl[i].fuelLayout = vhcl[j].fuelLayout;
			if(vhcl[j].fuel != 0){
				vhcl[i].fuelBar = vhcl[j].fuelBar;
			}
			vhcl[j].karakter = vhcl[h].karakter;
			vhcl[j].needed = vhcl[h].needed;
			vhcl[j].fuel = vhcl[h].fuel;
			vhcl[j].doc = vhcl[h].doc;
			vhcl[j].fuelLayout = vhcl[h].fuelLayout;
			if(vhcl[j].fuel != 0){
				vhcl[j].fuelBar = vhcl[h].fuelBar;
			}
			status2 = false;
			if(i>=3 && i < 5){
				vhcl[i].karakter->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 250,75)));
				vhcl[i].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 250,80 + vhcl[i].karakter->getContentSize().height/2)));
				if(vhcl[i].fuel != 0){
					vhcl[i].fuelBar->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 298,70 + vhcl[i].karakter->getContentSize().height/2)));
				}
				vhcl[j].karakter->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 100,75)));
				vhcl[j].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 100,80 + vhcl[j].karakter->getContentSize().height/2)));
				if(vhcl[j].fuel != 0){
					vhcl[j].fuelBar->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 148,70 + vhcl[j].karakter->getContentSize().height/2)));
				}
				totalCar--;
			}
			else if(i>=6){
				vhcl[i].karakter->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 300,winSize.height-350)));
				vhcl[i].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 300,winSize.height - 350 + vhcl[i].karakter->getContentSize().height/2)));
				if(vhcl[i].fuel != 0){
					vhcl[i].fuelBar->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 348,winSize.height - 360 + vhcl[i].karakter->getContentSize().height/2)));
				}
				totalPlane--;
			}	
			else if(i<3){
				vhcl[i].karakter->runAction(CCMoveTo::create(0.5,ccp(175,400)));
				vhcl[i].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(175,400 + vhcl[i].karakter->getContentSize().height/2)));
				if(vhcl[i].fuel != 0){
					vhcl[i].fuelBar->runAction(CCMoveTo::create(0.5,ccp(127,390 + vhcl[i].karakter->getContentSize().height/2)));
				}
				vhcl[j].karakter->runAction(CCMoveTo::create(0.5,ccp(300,250)));
				vhcl[j].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(300,250 + vhcl[j].karakter->getContentSize().height/2)));
				if(vhcl[j].fuel != 0){
					vhcl[j].fuelBar->runAction(CCMoveTo::create(0.5,ccp(252,240 + vhcl[j].karakter->getContentSize().height/2)));
				}
				totalShip--;
			}
		}
		else{
			vhcl[i].fuel = 0;
			if(i>=3 && i < 6){
				totalCar--;
			}
			else if(i>=6){
				totalPlane--;
			}
			else if(i<3){
				totalShip--;
			}
		}
	}
	else if(j%3==2){
		bool status3 = false;
		if(i==1 && totalShip == 3){
			status3 = true;
		}
		else if(i==4 && totalCar == 3){
			status3 = true;
		}
		if(status3){
			vhcl[i].karakter = vhcl[j].karakter;
			vhcl[i].needed = vhcl[j].needed;
			vhcl[i].fuel = vhcl[j].fuel;
			vhcl[i].doc = vhcl[j].doc;
			vhcl[i].fuelLayout = vhcl[j].fuelLayout;
			if(vhcl[j].fuel != 0){
				vhcl[i].fuelBar = vhcl[j].fuelBar;
			}
			status3 = false;
			if(i>=3 && i < 5){
				vhcl[i].karakter->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 100,75)));
				vhcl[i].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 100,80 + vhcl[i].karakter->getContentSize().height/2)));
				if(vhcl[i].fuel != 0){
					vhcl[i].fuelBar->runAction(CCMoveTo::create(0.5,ccp(winSize.width - 148,70 + vhcl[i].karakter->getContentSize().height/2)));
				}
				totalCar--;
			}
			else if(i<3){
				vhcl[i].karakter->runAction(CCMoveTo::create(0.5,ccp(300,250)));
				vhcl[i].fuelLayout->runAction(CCMoveTo::create(0.5,ccp(300,250 + vhcl[i].karakter->getContentSize().height/2)));
				if(vhcl[i].fuel != 0){
					vhcl[i].fuelBar->runAction(CCMoveTo::create(0.5,ccp(252,24 + vhcl[i].karakter->getContentSize().height/2)));
				}
				totalShip--;
			}
		}else{
			vhcl[i].fuel = 0;
			if(i>=3 && i <= 5){
				totalCar--;
			}
			else if(i>=6){
				totalPlane--;
			}
			else if(i<3){
				totalShip--;
			}
		}
	}else if (j%3 == 0){
		vhcl[i].fuel = 0;
		if(i>=3 && i <=5){
			totalCar--;
		}
		else if(i>=6){
			totalPlane--;
		}
		else if(i<3){
			totalShip--;
		}
	}
}


void HelloWorld::ShipGo(int i){
	vhcl[i].karakter->setFlipX(false);
	CCFiniteTimeAction* endMove = CCCallFuncN::create( this, callfuncN_selector(HelloWorld::spriteMoveFinished));
	CCFiniteTimeAction* startMove = CCMoveTo::create(1,ccp(0 - 200,400));
	vhcl[i].karakter->runAction(CCSequence::create(startMove,endMove,NULL));
	this->initDummyBug(i);
}

void HelloWorld::PlaneGo(int i){
	vhcl[i].karakter->setFlipX(true);
	CCFiniteTimeAction* endMove = CCCallFuncN::create( this, callfuncN_selector(HelloWorld::spriteMoveFinished));
	CCFiniteTimeAction* startMove = CCMoveTo::create(1,ccp(winSize.width + 200,winSize.height-350));
	vhcl[i].karakter->runAction(CCSequence::create(startMove,endMove,NULL));
	this->initDummyBug(i);
}

void HelloWorld::CarGo(int i){
	CCFiniteTimeAction* endMove = CCCallFuncN::create( this, callfuncN_selector(HelloWorld::spriteMoveFinished));
	CCFiniteTimeAction* startMove = CCMoveTo::create(1,ccp(winSize.width - 300, -100));
	vhcl[i].karakter->runAction(CCSequence::create(startMove,endMove,NULL));
	this->initDummyBug(i);

}

void HelloWorld::cekKlik(CCPoint location){
	for(int i=0;i<totalOil;i++){
		if(oil[i].karakter->boundingBox().containsPoint(location) == true){
			activatedOil = i;
		}
	}
}

void HelloWorld::initDummyBug(int arr){
	vhcl[arr].karakter = CCSprite::create("Images/bensin_lompat.png",CCRectMake(0,0,51,38));
	vhcl[arr].karakter->setPosition(ccp(-100,-100));
	this->addChild(vhcl[arr].karakter,1);
}

void HelloWorld::initAnim(){
	//Avtur
	_avturMembal = CCAnimation::create();

	for (int i = 1; i < 5; i++)
	{
		char szImageFileName[128] = {0};
		sprintf(szImageFileName, "Images/avtur_Lompat%d.oil", i);
		_avturMembal->addSpriteFrameWithFileName(szImageFileName);  
	}
	_avturMembal->setDelayPerUnit(1.0f / 5.0f);
	_avturMembal->setRestoreOriginalFrame(true);
}