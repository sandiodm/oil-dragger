#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "time.h"
#include "Box2D/Box2D.h"

#include "SimpleAudioEngine.h"

class HelloWorld : public cocos2d::CCLayerColor
{
public:
    virtual bool init();  
	void addTarget();
    static cocos2d::CCScene* scene();
    void menuCloseCallback(CCObject* pSender);
    CREATE_FUNC(HelloWorld);
	//Struct
	typedef struct charOil{
		cocos2d::CCSprite* karakter;
		int color;
		char nama[10];
	}charOil;
	charOil oil[20];
	typedef struct vehicle{
		cocos2d::CCSprite* karakter;
		int needed;
		int doc;
		int fuel;
		cocos2d::CCSprite* fuelLayout;
		cocos2d::CCSprite* fuelBar;
	}vehicle;
	vehicle vhcl[8];


protected :
	void spawnShip();
	void ShipGo(int i);
	void spawnCar();
	void idle();
	void CarGo(int i);
	void spawnPlane();
	void PlaneGo(int i);
	void levelGenerator();
	void oilLoop();
	void callObject();
	int activatedOil;
	cocos2d::CCSize winSize;

private :
	int jumlahOil,totalOil,finishedObject,totalObject,maxObject;
	int totalShip,totalCar,totalPlane,score;
	int spawnSpeed;
	float timer;
	bool next;
	cocos2d::CCLabelTTF* nilai;
	cocos2d::CCSprite *countdown;
	void tampilScore();
	void spriteMoveFinished(CCNode* sender);
	void gameLogic(float dt);
	void oilAlwaysMove(cocos2d::CCSprite* sender,int arr);
	void oilSuddenStop(cocos2d::CCSprite* sender,int arr);
	void cekKlik(cocos2d::CCPoint location);
	void fuelCek(int i);
	void timerMethod(float dt);
	void pumpUpFuel(int arr);
	void addNewOil();
	void initChar();
	void initDummyBug(int arr);
	void happyOil();
	void removeOil();
	void ekspresiSenang(int arr);
	void ekspresiNormal(int arr);
	void ekspresiSuram(int arr);
	void initAnim();
	//TouchSection
	void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
	void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
	void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);

	//Animation
	cocos2d::CCAnimation* _bensinMembal;
	cocos2d::CCAnimation* _solarMembal;
	cocos2d::CCAnimation* _avturMembal;
};

#endif